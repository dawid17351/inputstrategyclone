import java.util.Random;

public class RandomStrategy implements IInputStrategy {
    private Random random;

    public RandomStrategy() {
        this.random = new Random();
    }

    @Override
    public String getString() {
        return "hahaha, wygenerowalem stringa";
    }

    @Override
    public double getDouble() {
        return random.nextDouble();
    }

    @Override
    public int getInt() {
        return random.nextInt();
    }
}
